# Redefined standard Rails tasks only in instance mode
unless File.directory? "#{Merb.root}/app"
  require 'rake/testtask'
  
  ENV['MERB_RADIANT_ENV_FILE'] = File.join(Merb.root, 'config', 'environment')
  
  Dir["#{MERB_RADIANT_ROOT}/vendor/rails/railties/lib/tasks/*.rake", "#{MERB_RADIANT_ROOT}/vendor/plugins/rspec_on_rails/tasks/*.rake"].each do |rake|
    lines = IO.readlines(rake)
    lines.map! do |line|
      line.gsub!('Merb.root', 'MERB_RADIANT_ROOT') unless rake =~ /(misc|rspec)\.rake$/
      case rake
      when /testing\.rake$/
        line.gsub!(/t.libs << (["'])/, 't.libs << \1' + MERB_RADIANT_ROOT + '/')
        line.gsub!(/t\.pattern = (["'])/, 't.pattern = \1' + MERB_RADIANT_ROOT + '/')
      when /databases\.rake$/
        line.gsub!(/migrate\((["'])/, 'migrate(\1' + MERB_RADIANT_ROOT + '/')
        line.sub!("db/schema.rb", "#{Merb.root}/db/schema.rb")
      when /rspec\.rake$/
        line.gsub!('Merb.root', 'MERB_RADIANT_ROOT') unless line =~ /:noop/
        line.gsub!(/FileList\[(["'])/, "FileList[\\1#{MERB_RADIANT_ROOT}/")
      end
      line
    end
    eval(lines.join("\n"), binding, rake)
  end
end         
