class Admin::Extensions < Application
  def index
    @extensions = MerbRadiant::Extension.descendants.sort_by { |e| e.extension_name }
  end
end
