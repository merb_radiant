merb\_radiant README
===================

merb\_radiant is the codename for a port of popular Radiant Content Management 
System (CMS) to the merb MVC framework.

**WARNING** : merb\_radiant is in early porting stage, and should only be 
considered for consumption by reasonably advanced Ruby and merb developers.

Features of merb\_radiant include:

  * an elegant user interface
  * the ability to arrange pages in a hierarchy
  * a flexible templating system providing layouts, snippets, page parts, 
    and a custom tagging language (Radius: <http://radius.rubyforge.org>)  
  * a simple user management and permissions system
  * support for both the Markdown and Textile syntaxes, vanilla HTML, and the
    ability to develop and plug in your own alternative filters.
  * an advanced plugin system
  * operation in two modes: development and production, depending on the URL
  * a caching system which expires pages every 5 minutes
  * built on the cutting-edge, fast Merb framework.
  * and much more...

Installation and Setup
----------------------

Please see the INSTALL file details on getting merb\_radiant up and running.

Getting the Source
------------------

Performing a git clone of one of the following repositories will get you the
latest source:

    git clone git://github.com/myabc/merb_radiant.git
    git clone git://gitorious.org/merb_radiant/myabc-clone.git (on gitorious)

    git clone git://github.com/sbusso/merb_radiant.git
    git clone git://gitorious.org/merb_radiant/mainline.git (on gitorious)

The following additional mirrors are available:

    git://repo.or.cz/merb_radiant.git
    http://repo.or.cz/r/merb_radiant.git

Licensing and Copyright
-----------------------

merb\_radiant is released under an **MIT license**. Copyright information, as 
well as a copy of the MIT license may found in the LICENSE.txt file.

Support
-------

merb\_radiant is in early porting stage, so full support infrastructure is not
yet in place.

 * For now, please check out the porting wiki: 
  <http://github.com/sbusso/merb_radiant/wikis>

 * You may also contact the developers directly:
   - <alex@alexcolesportfolio.com>

You can also try the Radiant CMS mailing list for information relating to
Radiant CMS, the original Ruby on Rails-powered CMS:
<http://radiantcms.org/mailing-list/>

